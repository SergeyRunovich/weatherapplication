# Weather Application #



### Features: ###

* Weather display
* Weather characteristics
* location of the selected city
* Saving weather in realm

### Screenshots ###



![Scheme](https://bitbucket.org/repo/r9bEX6j/images/2065869027-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202021-04-14%20%D0%B2%2022.40.19.png)
![Scheme](https://bitbucket.org/repo/r9bEX6j/images/768692799-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202021-04-14%20%D0%B2%2022.39.35.png)
![Scheme](https://bitbucket.org/repo/r9bEX6j/images/256680228-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202021-04-14%20%D0%B2%2022.39.48.png)


### Libraries ###

* RealmSwift




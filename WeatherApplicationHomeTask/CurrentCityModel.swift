import Foundation
import RealmSwift



//// MARK: - JSONModel
//struct CurrentWeatherData: Codable {
//    let coord: Coord?
//    let weather: [Weather]?
//    let base: String?
//    let main: MainWeather?
//    let visibility: Int?
//    let wind: Wind?
//    let clouds: Clouds?
//    let dt: Int?
//    let sys: Sys?
//    let timezone: Int?
//    let id: Int?
//    let name: String?
//    let cod: Int?
//
//
//}
//
//// MARK: - Clouds
//struct Clouds: Codable {
//    let all: Int?
//
//}
//
//// MARK: - Coord
//struct Coord: Codable {
//    let lon: Double?
//    let lat: Double?
//
//}
//
//// MARK: - Main
//struct MainWeather: Codable {
//    let temp: Double?
//    let feelsLike: Double?
//    let tempMin: Double?
//    let tempMax: Double?
//    let pressure: Int?
//    let humidity: Int?
//
//}
//
//// MARK: - Sys
//struct Sys: Codable {
//    let type: Int?
//    let id: Int?
//    let country: String?
//    let sunrise: Int?
//    let sunset: Int?
//
//}
//
//
//// MARK: - Wind
//struct Wind: Codable {
//    let speed: Double?
//    let deg: Int?
//
//}


// MARK: - JSONModel
class CurrentWeatherData: Object, Codable {
    @objc dynamic var coord: Coord? = Coord()
    @objc dynamic var dt: Int = 0
  

}



// MARK: - Coord
class Coord: Object, Codable {
    @objc dynamic var lon: Double = 0
    @objc dynamic var lat: Double = 0


}










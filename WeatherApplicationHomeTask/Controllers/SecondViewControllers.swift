//
//  SecondViewControllers.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 1.03.21.
//

import UIKit

class SecondViewController: UIViewController {
    
    var dataFetcherService = NetworkDataFetcher()
    var currentWeatherData: WeatherData?
    var currentCityData: CurrentWeatherData?
    var currentCity = "Minsk"

    @IBOutlet weak var nextTable: UITableView!{
        didSet {
            let nib = UINib(nibName: "NextTableViewCell", bundle: nil)
            nextTable.register(nib, forCellReuseIdentifier: "cell")
            nextTable.delegate = self
            nextTable.dataSource = self
            
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        // Do any additional setup after loading the view.
    }
  
    
   
    
    func fetchCurrentCityData() {
        dataFetcherService.fetchWeatherForWeek(currentCityData?.coord?.lat ?? 1, currentCityData?.coord?.lon ?? 1) { [weak self] (weather) in
            guard let self = self else { return }
            self.currentWeatherData = weather
            DispatchQueue.main.async {
                self.nextTable.reloadData()
            }
        }
    }
    
    func fetchData() {
        dataFetcherService.fetchCurrentCityWeather(currentCity) { [weak self] (currentWeather) in
            self?.currentCityData = currentWeather
            self?.fetchCurrentCityData()
            DispatchQueue.main.async {
                self?.changeBackgroundImage()
                self?.nextTable.reloadData()
            }
        }
    }
    
    func changeBackgroundImage() {
        
        let date = Date(timeIntervalSince1970: Double(currentCityData?.dt ?? 1))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+2")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm"
        let strDate = dateFormatter.string(from: date)
        if strDate > "18:00" || strDate < "06:00" {
           nextTable.backgroundView = UIImageView(image: UIImage(named: "night"))
        } else {
           nextTable.backgroundView = UIImageView(image: UIImage(named: "clear"))
        }
    }
    
    
   
}


extension SecondViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NextTableViewCell
            headerCell.configure(currentWeatherData?.current)
            return headerCell
 
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 600
}

}


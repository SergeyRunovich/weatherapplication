//
//  MapViewController.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 11.03.21.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {
let locationManager = CLLocationManager()
    
    lazy var locationManagerr: CLLocationManager = {
        var manager = CLLocationManager()
        manager.distanceFilter = 10
        manager.desiredAccuracy = kCLLocationAccuracyBest
        return manager
    }()
    
    @IBOutlet weak var mapKit: MKMapView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //checkLocationEnabled()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setMark(lat: CityLocation.shared.location?.lat ?? 1, lon: CityLocation.shared.location?.lon ?? 1, cityName: CityLocation.shared.location?.name ?? "", temp: CityLocation.shared.location?.temp ?? 1)
    }
    
    
    
    func checkLocationEnabled() {
        if CLLocationManager.locationServicesEnabled() {
            checkAuthorization()
            setupManager()
        } else {
                let alert = UIAlertController(title: "У вас выключена служба геолокации", message: "Хотите включить?", preferredStyle: .alert)
            let seetingAction = UIAlertAction(title: "Настройки", style: .default) { (alert) in
                if let url = URL( string: "App-Prefs:root=LOCATION-SERVICES"){
                    UIApplication.shared.open(url, options:[:] , completionHandler: nil)
                }
            }
            let cancelAction = UIAlertAction(title: "Отмена", style: .cancel , handler: nil)
            
            alert.addAction(seetingAction)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
        }
    }
    func setupManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func setMark(lat: CLLocationDegrees, lon: CLLocationDegrees, cityName: String, temp: Double) {
        let sourceLocation = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let sourceAnnotaion = MKPointAnnotation()
        sourceAnnotaion.title = "\(cityName) Temp:\(temp)"
        
        
        if let location = sourcePlacemark.location {
            sourceAnnotaion.coordinate = location.coordinate
        }
        
        self.mapKit.showAnnotations([sourceAnnotaion], animated: true)
        
    }

   
    func checkAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            mapKit.showsUserLocation = true
            locationManager.startUpdatingLocation()
            
        default:
            break
        }
    }
    
    

}


extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last?.coordinate{
            let region = MKCoordinateRegion(center: location, latitudinalMeters: 50000, longitudinalMeters: 50000)
            mapKit.setRegion(region, animated: true)
    }
    
    }
    }
    

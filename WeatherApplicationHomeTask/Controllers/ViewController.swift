//
//  ViewController.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 1.03.21.
//


import UIKit
import CoreLocation
import RealmSwift 



class ViewController: UIViewController {
    
    
    lazy var locationManager: CLLocationManager = {
        var manager = CLLocationManager()
        manager.distanceFilter = 10
        manager.desiredAccuracy = kCLLocationAccuracyBest
        return manager
    }()
    
    
    let geoCoder = CLGeocoder()
    var dataFetcherService = NetworkDataFetcher()
    var currentWeatherData: WeatherData?
    var currentCityData: CurrentWeatherData?
    var currentCity = "London"
    @IBOutlet weak var mainTableView: UITableView! {
        didSet {
            let nib = UINib(nibName: "HeaderTableViewCell", bundle: nil)
            let otherNib = UINib(nibName: "WeekOfDayTableView", bundle: nil)
            let collectionNib = UINib(nibName: "TableViewCellForCollectionView", bundle: nil)
            mainTableView.register(otherNib, forCellReuseIdentifier: "otherCell")
            mainTableView.register(nib, forCellReuseIdentifier: "headerCell")
            mainTableView.register(collectionNib, forCellReuseIdentifier: "collectionCell")
            mainTableView.delegate = self
            mainTableView.dataSource = self
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    func fetchCurrentCityData() {
        dataFetcherService.fetchWeatherForWeek(currentCityData?.coord?.lat ?? 1, currentCityData?.coord?.lon ?? 1) { [weak self] (weather) in
            
            guard let self = self else { return }
            CityLocation.shared.location?.temp = weather?.current?.temp ?? 1
            if let currentWeather = weather {
                DispatchQueue.main.async {
                    do {
                        
                        print(Realm.Configuration.defaultConfiguration.fileURL)
                        let realm = try Realm()
                        let object = realm.objects(WeatherData.self)
                        try realm.write {
                            realm.add(currentWeather)
                        }
                        
                        try realm.write {
                            //realm.deleteAll()
                            //realm.delete(object)
                        }
                    } catch {
                        print("1")
                    }
                    
                }
            }
           
            self.currentWeatherData = weather
            DispatchQueue.main.async {
                self.mainTableView.reloadData()
            }
        }
    }
    
    func fetchData() {
        dataFetcherService.fetchCurrentCityWeather(currentCity) { [weak self] (currentWeather) in
            print("\(currentWeather) 111")
            self?.currentCityData = currentWeather
            self?.fetchCurrentCityData()
            DispatchQueue.main.async {
                self?.changeBackgroundImage()
                self?.mainTableView.reloadData()
            }
        }
    }
    
    func changeBackgroundImage() {
        
        let date = Date(timeIntervalSince1970: Double(currentCityData?.dt ?? 1))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+2")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm"
        let strDate = dateFormatter.string(from: date)
        if strDate > "18:00" || strDate < "06:00" {
            mainTableView.backgroundView = UIImageView(image: UIImage(named: "night"))
        } else {
            mainTableView.backgroundView = UIImageView(image: UIImage(named: "clear"))
        }
    }
    
    
   
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentWeatherData?.daily.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as! HeaderTableViewCell
            headerCell.configure(currentWeatherData?.current)
            headerCell.cityLabel.text = currentCity
            return headerCell
        case 1:
            let collectionViewCell = tableView.dequeueReusableCell(withIdentifier: "collectionCell", for: indexPath) as! SecondTableViewCell
            collectionViewCell.count = currentWeatherData?.hourly.count
            collectionViewCell.weatherData = currentWeatherData?.hourly
            collectionViewCell.currentDayData = currentWeatherData?.current
            
            return collectionViewCell
        default:
            let dayOfWeekCell = tableView.dequeueReusableCell(withIdentifier: "otherCell", for: indexPath) as! WeekOfDayTableViewCell
            let someWeatherData = currentWeatherData?.daily[indexPath.row]
            dayOfWeekCell.configure(someWeatherData, currentWeatherData?.current)
            return dayOfWeekCell
            
        }
 
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 300
        } else if indexPath.row == 1{
            return 150
        } else {
            return 100
        }
    }
}




extension ViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
        
    
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
    
        geoCoder.reverseGeocodeLocation(location) { (placemarks, _) in
            
            placemarks?.forEach({ (placemark) in
                if let city = placemark.locality {
                   
                    CityLocation.shared.location = Location(lat: location.coordinate.latitude, lon: location.coordinate.longitude, name: city)
                    self.currentCity = city
                    self.fetchData()
                    print(city)}
                
            })
        }

    }
    
}

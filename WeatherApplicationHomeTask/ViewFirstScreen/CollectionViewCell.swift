//
//  CollectionViewCell.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 1.03.21.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
    func changeToNight() {
        timeLabel.textColor = .white
        temperatureLabel.textColor = .white
        
    }
    
    func configure(_ result: HourlWeather?, _ currentDay: Current?) {
        
        guard let currentWeather = result else { return }
        
        let date = Date(timeIntervalSince1970: Double(currentWeather.dt ?? 1))
        let currentDate = Date(timeIntervalSince1970: Double(currentDay?.dt ?? 1))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+2")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm"
        let currentTime = dateFormatter.string(from: currentDate)
        let strDate = dateFormatter.string(from: date)
        self.timeLabel.text = strDate
        self.temperatureLabel.text = "\(String(format: "%.0f", currentWeather.temp ?? 1))º"
        
        let someWeatherData = currentWeather.weather[0]
        
      
        if someWeatherData.main == "Clear" && currentTime > "18:00"  {
            weatherImageView.image = UIImage(named: "moon")
        } else {
            weatherImageView.image = UIImage(named: "sunny")
        }
        
        switch someWeatherData.main {
        case "Snow":
            weatherImageView.image = UIImage(named: "snow")
        case "Rain":
            weatherImageView.image = UIImage(named: "rain")
        case "Clouds":
            weatherImageView.image = UIImage(named: "clouds")
        default:
            break
        }
        
        if currentTime > "18:00" {
            changeToNight()
        }
        
        
    }
}

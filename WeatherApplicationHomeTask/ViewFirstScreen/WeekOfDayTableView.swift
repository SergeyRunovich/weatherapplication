//
//  WeekOfDayTableView.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 1.03.21.
//

import UIKit

class WeekOfDayTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var dayOfWeekLabel: UILabel!
    @IBOutlet weak var maxTempOfDay: UILabel!
    @IBOutlet weak var minTempOfDay: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
    func changeToNight() {
        dayOfWeekLabel.textColor = .white
        maxTempOfDay.textColor = .white
        minTempOfDay.textColor = .white
        dateLabel.textColor = .white
    }
    
    func configure(_ result: Daily?, _ currentDate: Current?) {
        
        guard let currentWeather = result else { return }
        
        let date = Date(timeIntervalSince1970: Double(currentWeather.dt ?? 1))
        let currentDate = Date(timeIntervalSince1970: Double(currentDate?.dt ?? 1))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+2")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "EEEE"
        let dayOfWeekString = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "dd.MM"
        let dataOfDay = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "HH:mm"
        let currentTime = dateFormatter.string(from: currentDate)
        dateLabel.text = dataOfDay
        dayOfWeekLabel.text = dayOfWeekString
        maxTempOfDay.text = "\(String(format: "%.0f", currentWeather.temp?.max ?? 1))º"
        minTempOfDay.text = "\(String(format: "%.0f", currentWeather.temp?.min ?? 1))º"
        
        
        let someWeatherData = currentWeather.weather[0]
        
        switch someWeatherData.main {
        case "Clear":
            weatherImageView.image = UIImage(named: "sunny")
        case "Snow":
            weatherImageView.image = UIImage(named: "snow")
        case "Rain":
            weatherImageView.image = UIImage(named: "rain")
        case "Clouds":
            weatherImageView.image = UIImage(named: "clouds")
        default:
            break
        }
        if currentTime > "18:00" {
            changeToNight()
        }
        
    }
    
}

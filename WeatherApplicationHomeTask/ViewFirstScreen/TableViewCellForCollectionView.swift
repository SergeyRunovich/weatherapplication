//
//  TableViewCellForCollectionView.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 1.03.21.
//

import UIKit
import RealmSwift
class SecondTableViewCell: UITableViewCell {
    
    
    var count: Int?
    var weatherData: List<HourlWeather>?
    var currentDayData: Current?
    @IBOutlet weak var mainCollectionView:
        UICollectionView! {
        didSet {
            let nib = UINib(nibName: "CollectionViewCell", bundle: nil)
            
            mainCollectionView.register(nib, forCellWithReuseIdentifier: "collCell")
            mainCollectionView.delegate = self
            mainCollectionView.dataSource = self
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
    
    
}

extension SecondTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count ?? 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collCell", for: indexPath) as! CollectionViewCell
        cell.configure(weatherData?[indexPath.row], currentDayData)
        return cell
        
    }
    
    
}

//
//  HeaderTableViewCell.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 1.03.21.
//

import UIKit


class HeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var mainTemperatureLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var upTempLabel: UILabel!
    @IBOutlet weak var downTempLabel: UILabel!
    var callBack : (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        hideView(true)
        backgroundColor = .clear
        
        
        
    }
    
    
    func hideView(_ hidden: Bool) {
        cityLabel.isHidden = hidden
        timeLabel.isHidden = hidden
        mainTemperatureLabel.isHidden = hidden
        weatherLabel.isHidden = hidden
        upTempLabel.isHidden = hidden
        downTempLabel.isHidden = hidden
    }
    
    func changeToNight() {
        cityLabel.textColor = .white
        timeLabel.textColor = .white
        mainTemperatureLabel.textColor = .white
        weatherLabel.textColor = .white
        upTempLabel.textColor = .white
        downTempLabel.textColor = .white
    }
    
    
    
    func configure(_ result: Current?) {
        
        guard let currentWeather = result else { return }
        
        let date = Date(timeIntervalSince1970: Double(currentWeather.dt ?? 1))
        let sunDownDate = Date(timeIntervalSince1970: Double(currentWeather.sunrise ?? 1))
        let sunUpDate = Date(timeIntervalSince1970: Double(currentWeather.sunset ?? 1))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+2")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm"
        let strDate = dateFormatter.string(from: date)
        let sunDowntime = dateFormatter.string(from: sunDownDate)
        let sunUpTime = dateFormatter.string(from: sunUpDate)
        self.timeLabel.text = strDate
        self.mainTemperatureLabel.text = "\(String(format: "%.0f", currentWeather.temp ?? 1))º"
        self.weatherLabel.text = currentWeather.weather[0].main
        self.upTempLabel.text = "↑\(sunDowntime)"
        self.downTempLabel.text = "↓\(sunUpTime)"
        if strDate > "18:00" {
            changeToNight()
        }
        
        hideView(false)
    }
    
    
    
}

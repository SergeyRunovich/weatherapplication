//
//  Enums + API.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 1.03.21.
//

import Foundation

enum API: String {
    
    case baseUrl = "https://api.openweathermap.org/data/2.5/onecall?lat="
    case lon = "&lon="
    case propAndApiKey = "&exclude=minutely&appid=19f4ab338a14a90e3d170eaeaa5f6383&units=metric"
    case apikey = "&appid=19f4ab338a14a90e3d170eaeaa5f6383"
    case currentWeatherUrl = "https://api.openweathermap.org/data/2.5/weather?q="
}

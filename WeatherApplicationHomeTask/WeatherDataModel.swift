import Foundation
import RealmSwift

// MARK: - JSONModel
class WeatherData : Object , Codable {
    @objc dynamic var current: Current? = Current()
    var hourly = List<HourlWeather>()
    var daily = List<Daily>()
    
}



// MARK: - Current
class HourlWeather : Object , Codable {
    @objc dynamic var dt: Int = 0
    @objc dynamic var temp: Double = 0.0
    //@objc dynamic var feelsLike: Double = 0.0
    @objc dynamic var pressure: Int = 0
    @objc dynamic var humidity: Int = 0
    //@objc dynamic var dewPoint: Double = 0.0
    @objc dynamic var uvi: Double = 0.0
    @objc dynamic var clouds: Int = 0
    @objc dynamic var visibility: Int = 0
    @objc dynamic var wind_speed: Double = 0.0
    //@objc dynamic var windDeg: Int = 0
    var weather = List<Weather>()
   
}
class Current:  Object, Codable {
    @objc dynamic var dt: Int = 0
    @objc dynamic var sunrise: Int = 0
    @objc dynamic var sunset: Int = 0
    @objc dynamic var temp: Double = 0.0
    //@objc dynamic var feelsLike: Double = 0.0
    @objc dynamic var humidity: Int = 0
    //@objc dynamic var dewPoint: Double = 0.0
    @objc dynamic var uvi: Double = 0.0
    //@objc dynamic var clouds: Int = 0
    @objc dynamic var visibility: Int = 0
    @objc dynamic var wind_speed: Double = 0.0
    @objc dynamic var pressure: Int = 0
    //@objc dynamic var windDeg: Int = 0
    var weather = List<Weather>()
    //@objc dynamic var windGust: Double = 0.0
    //@objc dynamic var pop: Double = 0.0
    //@objc dynamic var snow: Snow? = Snow()
//    enum CodingKeys:  String, CodingKey {
//        case feelsLike = "feels_like"
//     }
    }



// MARK: - Snow
//class Snow:  Object, Codable {
//    @objc dynamic var the1H: Double = 0.0
//
//    enum CodingKeys: String, CodingKey {
//        case the1H
//    }
//}

 //MARK: - Weather
class Weather: Object, Codable {
    @objc dynamic var id: Int = 0
    @objc dynamic var main: String = ""
    //@objc dynamic var description: String = ""
    @objc dynamic var icon: String = ""

}


// MARK: - Daily
class  Daily: Object, Codable {
    
    @objc dynamic var dt: Int = 0
    @objc dynamic var sunrise: Int = 0
    @objc dynamic var sunset: Int = 0
    @objc dynamic var temp: Temp? = Temp()
    @objc dynamic var feelsLike: FeelsLike? = FeelsLike()
    @objc dynamic var pressure: Int = 0
    @objc dynamic var humidity: Int = 0
    //@objc dynamic var dewPoint: Double = 0.0
    @objc dynamic var clouds: Int = 0
    //@objc dynamic var windSpeed: Double = 0.0
    //@objc dynamic var windDeg: Int = 0
    var weather = List<Weather>()
    @objc dynamic var pop: Double = 0.0
    //@objc dynamic var snow: Snow? = Snow()
    //@objc dynamic var rain: Double = 0.0
    @objc dynamic var uvi: Double = 0.0
    
}


// MARK: - FeelsLike
class FeelsLike: Object, Codable {
    @objc dynamic var day: Double = 0.0
    @objc dynamic var night: Double = 0.0
    @objc dynamic var eve: Double = 0.0
    @objc dynamic var morn: Double = 0.0
    

}


// MARK: - Temp
class Temp: Object , Codable {
    @objc dynamic var day: Double = 0.0
    @objc dynamic var night: Double = 0.0
    @objc dynamic var eve: Double = 0.0
    @objc dynamic var morn: Double = 0.0
    @objc dynamic var min: Double = 0.0
    @objc dynamic var max: Double = 0.0
}



//
//  CityLocation.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 14.03.21.
//

import Foundation
import CoreLocation

struct Location {
    let lat: CLLocationDegrees
    let lon: CLLocationDegrees
    let name: String
    var temp: Double = 1
}


class CityLocation {
    
    static var shared = CityLocation()
    
    var location: Location?
    
    private init() {}
    
    
}

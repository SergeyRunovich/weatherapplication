//
//  NetworkDataFetcher.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 1.03.21.
//

import Foundation
class NetworkDataFetcher {
    
    
    var dataFetcher: DataFetcher?
    
    init(dataFetcher: DataFetcher = NetworkManager()) {
        self.dataFetcher = dataFetcher
    }
    
    
    func fetchWeatherForWeek(_ lat: Double,_ lon: Double, _ completionHandler: @escaping (WeatherData?) -> Void) {
        let url = URL(string: "\(API.baseUrl.rawValue)\(lat)\(API.lon.rawValue)\(lon)\(API.propAndApiKey.rawValue)")
       print(url)
        dataFetcher?.fetchData(url: url, completionHandler)
    }
    
    func fetchCurrentCityWeather(_ quary: String,_ completionHandler: @escaping (CurrentWeatherData?) -> Void) {
        let url = URL(string:API.currentWeatherUrl.rawValue + quary + API.apikey.rawValue)
       
        dataFetcher?.fetchData(url: url, completionHandler)
    }
    
}

//
//  NetworkManager.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 1.03.21.
//

import Foundation
protocol DataFetcher {
    func fetchData<T: Codable>(url: URL?,_ response: @escaping (T?) -> Void)
}


class NetworkManager:DataFetcher {
    
    var networking: Networking?
    
    init(networking: Networking = NetworkService()) {
        self.networking = networking
    }
    
    func fetchData<T: Codable>(url: URL?,_ responce: @escaping (T?) -> Void) {
        
        networking?.request(url: url, completion: { (data, error) in
            if let error = error {
                print("\(error)")
                responce(nil)
            }
            if let data = data {
                let decoder = JSONDecoder()
                let objects = try? decoder.decode(T.self, from: data)
                responce(objects)
            }
            
            
        })
    }
    
}

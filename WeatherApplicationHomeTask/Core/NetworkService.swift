//
//  NetworkService.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 1.03.21.
//

import Foundation
protocol Networking {
    func request(url: URL?, completion: @escaping (Data?, Error?) -> Void)
}


class NetworkService: Networking {
    
    func request(url: URL?, completion: @escaping (Data?, Error?) -> Void) {
        if let url = url {
            let task = URLSession.shared.dataTask(with: url) { (data, responce, error) in
                if let responce = responce {
                    //
                }
                if let data = data {
                    completion(data,error)
                }
                
                if let error = error {
                    print(error)
                }
            }
            task.resume()
        }
    }
    
}

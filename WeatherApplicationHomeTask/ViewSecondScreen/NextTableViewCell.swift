//
//  NextTableViewCell.swift
//  WeatherApplicationHomeTask
//
//  Created by Сергей Рунович on 3.03.21.
//

import UIKit

class NextTableViewCell: UITableViewCell {
        
        @IBOutlet weak var timeLabel: UILabel!
        @IBOutlet weak var sunriseLabel: UILabel!
        @IBOutlet weak var chanceOfSnowLabel: UILabel!
        @IBOutlet weak var windLabel: UILabel!
        @IBOutlet weak var precipitationLabel: UILabel!
        @IBOutlet weak var visibilityLabel: UILabel!
        @IBOutlet weak var sunsenLabel: UILabel!
        
        @IBOutlet weak var humidityLabel: UILabel!
        @IBOutlet weak var feelsLikeLabel: UILabel!
        @IBOutlet weak var pressureLabel: UILabel!
        @IBOutlet weak var uvIndexLabel: UILabel!
        override func awakeFromNib() {
            super.awakeFromNib()
            hideView(true)
            backgroundColor = .clear
            
            
            
        }
        
        
        func hideView(_ hidden: Bool) {
            timeLabel.isHidden = hidden
            sunriseLabel.isHidden = hidden
            chanceOfSnowLabel.isHidden = hidden
            windLabel.isHidden = hidden
            precipitationLabel.isHidden = hidden
            visibilityLabel.isHidden = hidden
            sunsenLabel.isHidden = hidden
            humidityLabel.isHidden = hidden
            feelsLikeLabel.isHidden = hidden
            pressureLabel.isHidden = hidden
            uvIndexLabel.isHidden = hidden
        }
        
        func changeToNight() {
            
            timeLabel.textColor = .white
            sunriseLabel.textColor = .white
            chanceOfSnowLabel.textColor = .white
            windLabel.textColor = .white
            precipitationLabel.textColor = .white
            visibilityLabel.textColor = .white
            sunsenLabel.textColor = .white
            humidityLabel.textColor = .white
            feelsLikeLabel.textColor = .white
            pressureLabel.textColor = .white
            uvIndexLabel.textColor = .white

        }
        
        
        
        func configure(_ result: Current?) {
            
            guard let currentWeather = result else { return }
            
            let date = Date(timeIntervalSince1970: Double(currentWeather.dt ?? 1))
            let sunrise = Date(timeIntervalSince1970: Double(currentWeather.sunrise ?? 1))
            let sunsen = Date(timeIntervalSince1970: Double(currentWeather.sunset ?? 1))
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT+2")
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "HH:mm"
            let strDate = dateFormatter.string(from: date)
            let sunRice = dateFormatter.string(from:sunrise)
            let sunSen = dateFormatter.string(from: sunsen)
            self.sunriseLabel.text = sunRice
            self.sunsenLabel.text = sunSen
            self.timeLabel.text = strDate
            //self.chanceOfSnowLabel.text = "\(result?.pop ?? 1) %"
            self.humidityLabel.text = "\(result?.humidity ?? 1) %"
            self.windLabel.text = "\(result?.wind_speed ?? 1 * 3.6) km/h "
            //self.feelsLikeLabel.text = "\(result?.feelsLike ?? 1) º"
            //self.precipitationLabel.text = "\(result?.snow?.the1H ?? 1 / 10) cm "
            self.pressureLabel.text = "\(result?.pressure ?? 1) hPa"
            self.visibilityLabel.text = "\(result?.visibility.nonzeroBitCount ?? 1 / 10000 ) km"
            self.uvIndexLabel.text = "\(result?.uvi ?? 1) %"
            
            if strDate > "18:00" {
                changeToNight()
            }
            
            hideView(false)
        }
        
        
        
    }

    
    

